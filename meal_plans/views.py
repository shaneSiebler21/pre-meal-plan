from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView,UpdateView, DeleteView
from django.views.generic.list import ListView


from meal_plans.forms import MealPlanForm, MealPlanDeleteForm 
from meal_plans.models import MealPlan

# Create your views here.

class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 10

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
    

class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan 
    template_name = "meal_plan/new.html"
    fields = ["name", "recipes", "date"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan_detail" , pk=plan.id)

class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan 
    template_name = "meal_plan/new.html"
    fields = ["name", "recipes", "date"]

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail" , args =[self.objects.id])   

class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user) 








    
